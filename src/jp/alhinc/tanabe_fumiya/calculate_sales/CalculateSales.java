package jp.alhinc.tanabe_fumiya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {
		try {
			Map<String, String> map1 = new HashMap<String, String>();
			Map<String, Long> map2 = new HashMap<String, Long>();

			BufferedReader br = null;

			try {
				//支店定義ファイル("branch.lst")を格納フォルダより読込み
				File file = new File(args[0], "branch.lst");

				if (!file.exists()) {
					System.out.print("支店定義ファイルが存在しません");
					return;
				}
				br = new BufferedReader(new FileReader(file));

				String line;
				while ((line = br.readLine()) != null) {
					String[] str = line.split(",");
					if (str.length != 2 || !str[0].matches("^[0-9]{3}$")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					map1.put(str[0], str[1]); //(支店コード[0],支店名[1])
					map2.put(str[0], (long) 0); //(支店コード[0],金額（0円仮置き)
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				br.close();
			}

			//売上ファイルにおいてフィルター活用（”8桁”及び”.rcd”のみ出力
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {
					if (str.matches("^[0-9]{8}.rcd$")) {
						return true;
					} else {
						return false;
					}
				}
			};

			File[] list = new File(args[0]).listFiles(filter);
			List<Integer> branchNumber = new ArrayList<>();
			for (int l = 0; l < list.length; l++) {
				String fileName = list[l].getName();
				branchNumber.add(Integer.parseInt(fileName.substring(0, 8)));
			}
			Collections.sort(branchNumber);

			for (int n = 1; n < branchNumber.size(); n++) {
				if (branchNumber.get(n) != branchNumber.get(n - 1) + 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			;

			//売上の情報をファイルに出力する
			List<String> numberCheck = new ArrayList<String>(map1.keySet());
			for (int i = 0; i < list.length; i++) {
				try {
					FileReader fr1 = new FileReader(list[i]);
					br = new BufferedReader(fr1);
					String line1 = br.readLine(); //1行目を読み込む
					String line2 = br.readLine(); //2行目を読み込む
					String line3 = br.readLine();
					Long sales = Long.valueOf(line2);
					if (!(numberCheck.contains(line1))) {
						System.out.println(list[i].getName() + "の支店コードが不正です");
						return;
					}
					if (line3 != null) {
						System.out.println(list[i].getName() + "のフォーマットが不正です");
						return;
					}
					Long oldSales = map2.get(line1);
					Long total = oldSales + sales;
					if (total > 9999999999L) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					map2.put(line1, total); //map2(金額)上書き
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					br.close();
				}
			}
			FileWriter file1 = new FileWriter(new File(args[0], "branch.out"));
			PrintWriter pw = new PrintWriter(new BufferedWriter(file1));
			try {
				for (Entry<String, String> entry : map1.entrySet()) {
					pw.println(entry.getKey() + " ," + entry.getValue() + ", " + map2.get(entry.getKey()));
				}
			} finally {
				pw.close();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
		}
	}
}
